<?php
error_reporting(NULL);
$TAB = 'INSTALADOR';
$_SESSION['back'] = $_SERVER['REQUEST_URI'];

// Main include
include($_SERVER['DOCUMENT_ROOT']."/inc/main.php");

include($_SERVER['DOCUMENT_ROOT'].'/templates/header.html');
top_panel($user,$TAB);

// Javascript
echo ("<script>");
require_once('assets/js/main.js');
require_once('assets/js/jquery.min.js');
echo ("</script>");

// HTML
require_once('includes/f_auxiliares.php');
require_once('assets/html/add_app.html');

// Footer
include($_SERVER['DOCUMENT_ROOT'].'/templates/footer.html');

// Vesta Objects
include($_SERVER['DOCUMENT_ROOT'].'/templates/scripts.html');

?>
