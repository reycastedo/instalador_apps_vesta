<?php

function listar_dominios($user) {
    $misdominios = shell_exec(VESTA_CMD.'v-list-web-domains ' . $user .' json');
    $misdominios = json_decode($misdominios, true);
    ksort($misdominios);

    foreach( $misdominios as $dominio => $Array ) {
        echo '<option value="' . $dominio . '">' . $dominio . ' </option>';

        $listadoalias = explode(',', $misdominios["$dominio"]["ALIAS"]);

        foreach ( $listadoalias as $alias ) {
            echo '<option value="' . $alias . '">' . $alias . ' </option>';
        }
    }
}
