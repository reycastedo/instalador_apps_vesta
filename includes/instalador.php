<?php
// Main include
include($_SERVER['DOCUMENT_ROOT']."/inc/main.php");

$aplicacion = htmlspecialchars( $_POST["aplicacion"] );
$dominio = htmlspecialchars( $_POST["dominio"] );
$usuariocms = htmlspecialchars( $_POST["usuarioCMS"] );
$clavecms = htmlspecialchars( $_POST["cmsPass"] );
$email = htmlspecialchars( $_POST["correo"] );
$ssl = htmlspecialchars( $_POST["https"] );

/*
$aplicacion="wordpress";
$user="cuatro";
$dominio="dominio1.tld";
$usuariocms="fran";
$clavecms="sk8";
$email="juandunasterio@gmail.com";
$ssl="no";
*/


if ( empty($aplicacion) || empty($dominio) || empty($clavecms) || empty($email) || empty($ssl) ) {
    echo "Los datos enviados no son correctos \n";
    echo "1.$aplicacion \n";
    echo "2.$dominio \n";
    echo "3.$usuariocms \n";
    echo "4.$clavecms \n";
    echo "5.$email \n";
    echo "6.$ssl \n";
    echo "7.$user \n";
}
else {
    if ( $aplicacion == "wordpress" ) {
        $status = exec( "sudo /usr/local/vesta/bin/v-install-wordpress '$user' '$dominio' '$usuariocms' '$clavecms' '$email' '$ssl' 2>&1" );
        echo ( "$status" );
    }
    elseif ( $aplicacion == "prestashop" ) {
        $status = exec( "sudo /usr/local/vesta/bin/v-install-prestashop '$user' '$dominio' '$email' '$clavecms' '$ssl' 2>&1" );
        echo ( "$status" );
    }
    else {
        exit;
    }
}

