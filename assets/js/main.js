// Se ocultan los elementos del formulario que no se necesitan
function hideUnnecessary() {
    if ( $("#aplicacion option:selected").text() != "WordPress" ) {
        $("tr.wordpress").fadeOut()
        $("input.wordpress")
            .slideUp()
            .prop('required',false)
            .val("")
    }
    else {
	$("tr.wordpress").fadeIn()
        $("input.wordpress")
            .slideDown()
            .prop('required',true)
    }
}

// Compara el string en el campo de correo con una expresión regular para validarla
function validarEmail() {
    var correo = $("#correo").val();


    if ( ! /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/.test(correo) ) {
        $("#validar_mail").fadeIn()
        return false;
    }
    else {
        $("#validar_mail").fadeOut()
    }
};

// Valida que el usuario haya rellenado todos los campos e impide que el formulario continúe adelante si no lo ha hecho
function validarCampos(aplicacion, correo, cmsPass, usuarioCMS) {

    if ( ( aplicacion == "wordpress" ) && ( ( ! usuarioCMS ) || ( ! correo ) || ( ! cmsPass ) ) ) {
        return false;
    }

    if ( ( aplicacion == "prestashop" ) && ( ( ! correo ) || ( ! cmsPass ) ) ) {
        return false;
    }
};

// Gestiona la petición Ajax
function myAjax(dominio, aplicacion, correo, usuarioCMS, cmsPass, https) {

    $.post("includes/instalador.php",
        {
          dominio: dominio,
          aplicacion: aplicacion,
          correo: correo,
          usuarioCMS: usuarioCMS,
          cmsPass: cmsPass,
          https: https
        },
        function(data,status){
            alert("Data: " + data);
            
            $("#instalar")
             .prop("disabled",false)
             .text("Instalar")
             .css("background-color", "#9FBF0C")

            $("#loader").css("display", "none")

            location.reload(); 
    })
};

// Lógica principal
// Recoge los datos en el formulario y se encarga de realizar la instalación tras las validaciones
function instalarCMS() {
    var dominio = $("#dominio").val();
    var aplicacion = $("#aplicacion").val();
    var correo = $("#correo").val();
    var usuarioCMS = $("#usuario_cms").val();
    var cmsPass = $("#cms_pass").val();
    var https = $("#https").val();

    if ( validarEmail() == false ) {
        return;
    }

    if ( validarCampos( aplicacion, correo, cmsPass, usuarioCMS ) == false ) {
        return;
    }

   $("#instalar")
    .prop("disabled",true)
    /*.text("Instalando...")*/
    .text("")
    .append("<img src=../../../../images/load.gif  />")
    .css("background-color", "#FFFFFF")

    myAjax( dominio, aplicacion, correo, usuarioCMS, cmsPass, https )
};
